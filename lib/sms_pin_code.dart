import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
// import 'package:sms_consent/sms_consent.dart';

class SmsPinCode extends StatefulWidget {
  /// The textController which will hold the tapped code
  final TextEditingController controller;

  /// The focusNode of the widget
  final FocusNode focusNode;

  /// A `VoidCallback` to be called when the pin is completed
  final Function(String) onSubmitted;

  /// The background color of the fields
  final Color color;

  /// The number of the digits on the pin. Default: 6.
  final int digits;

  /// If the code is a sms code and should be filled automatically. Defaults to true if digits == 6.
  final bool isSmsCode;

  SmsPinCode({Key key, this.controller, this.focusNode, this.onSubmitted, this.color = Colors.white, this.digits = 6, this.isSmsCode}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _SmsPinCodeWidgetState();
}

class _SmsPinCodeWidgetState extends State<SmsPinCode> {
  static const double _SMS_BOX_WIDTH = 35;
  static const double _SMS_BOX_HEIGHT = 50;

  List<TextEditingController> _digitControllers;

  List<FocusNode> _digitFocus;

  _SmsPinCodeWidgetState();

  @override
  void initState() {
    super.initState();

    _digitControllers = List<TextEditingController>.generate(this.widget.digits, (_) => TextEditingController());
    _digitFocus = List<FocusNode>.generate(this.widget.digits, (_) => FocusNode());

    //If isSmsCode is not set and we have 6 digits we enable it automatically
    bool isSmsCode = widget.isSmsCode;
    if (isSmsCode == null) isSmsCode = widget.digits == 6;

    // Init the SMS User Consent API if android
    // if (Platform.isAndroid && isSmsCode) {
    //   SmsConsent.startSMSConsent().then((otp) {
    //     if (otp.isNotEmpty) {
    //       _setPlatformSMSCode(otp);
    //     }
    //   });
    // }

    _digitFocus.forEach((f) {
      f.addListener(() {
        if (f.hasFocus) {
          FocusScope.of(context).requestFocus(widget.focusNode);
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).requestFocus(widget.focusNode),
      child: Stack(
        children: <Widget>[
          Opacity(
            opacity: 0,
            child: Theme(
              data: ThemeData(splashColor: Colors.transparent),
              child: TextField(
                controller: widget.controller,
                decoration: InputDecoration(
                  counterText: '',
                ),
                focusNode: widget.focusNode,
                textAlign: TextAlign.center,
                keyboardType: TextInputType.numberWithOptions(),
                onChanged: (text) {
                  _fillCodeFields(text);

                  if (text.length == widget.digits) {
                    this.widget.onSubmitted(text);
                  }
                },
                onSubmitted: (text) {
                  if (text.length == widget.digits) {
                    this.widget.onSubmitted(text);
                  }
                },
                inputFormatters: [LengthLimitingTextInputFormatter(6)],
              ),
            ),
          ),
          Container(
            child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: Iterable<int>.generate(widget.digits)
                    .toList()
                    .asMap()
                    .map((index, value) {
                      return MapEntry(
                        index,
                        Padding(
                          //Add a padding to the left on all fields except the first
                          padding: EdgeInsets.only(left: index == 0 ? 0 : 5),
                          child: SizedBox(
                            width: _SMS_BOX_WIDTH,
                            height: _SMS_BOX_HEIGHT,
                            child: TextField(
                              controller: _digitControllers[index],
                              decoration: InputDecoration(
                                  hintText: '1',
                                  hintStyle: TextStyle(color: Colors.grey.withAlpha(100)),
                                  counterText: '',
                                  filled: true,
                                  fillColor: this.widget.color,
                                  disabledBorder: widget.focusNode.hasFocus && widget.controller.text.length == index ? _focusedBorder() : null),
                              enabled: false,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 20,
                              ),
                              maxLength: 1,
                              keyboardType: TextInputType.numberWithOptions(),
                              showCursor: true,
                              textInputAction: TextInputAction.next,
                              focusNode: _digitFocus[index],
                            ),
                          ),
                        ),
                      );
                    })
                    .values
                    .toList()),
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    // Stop the SMS User Consent API if android
    // if (Platform.isAndroid) {
    //   SmsConsent.stopSMSConsent();
    // }
    // Super call
    super.dispose();
  }

  void _fillCodeFields(String text) {
    Iterable<int>.generate(widget.digits).forEach((i) {
      setState(() {
        try {
          _digitControllers[i].text = text[i];
        } on RangeError catch (_) {
          _digitControllers[i].text = '';
        }
      });
    });
  }

  UnderlineInputBorder _focusedBorder() {
    return UnderlineInputBorder(borderSide: BorderSide(color: Theme.of(context).primaryColor, width: 2));
  }

  void _setPlatformSMSCode(String otp) {
    widget.controller.text = otp;
    _fillCodeFields(otp);
    if (otp.length == widget.digits) {
      this.widget.onSubmitted(otp);
    }
  }
}
