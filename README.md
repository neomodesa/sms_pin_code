# sms\_pin\_code

A pin code widget with can be automatically fullfiled with codes received via SMS both on Android and iOS.

Android codes are provided via the native `user_consent` API https://developers.google.com/identity/sms-retriever/user-consent/request

iOS are automatically provided by the system as an auto-complete suggestion over the keyboard.

## Getting Started

Just add the `SMSPinCode` at your widget hierarchy:

```dart
SmsPinCode(
	controller: _smsCodeController,
	focusNode: _smsCodeFocus,
	onSubmitted: (text) => _confirmSmsTap()
)
```

##### Parameters

- `controller`: A regular `TextEditingController` which will hold the entire input pin code on a single string.
- `focusNode`: The focus of the SMS Pin Code. Useful to focus on it automatically after the sms is sent.
- `onSubmitted`: A `VoidCallback` to be called when the pin is completed.
- `color`: The background color of the input.
- `digits`: The number of digits of the PIN. Defaults to `6`.
- `isSmsCode`: If SMS Code completion should be enabled. Defaults to true when `digits == 6`.